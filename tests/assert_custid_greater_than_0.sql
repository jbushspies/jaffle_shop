-- customers with id < 0, so the customer id should always be >= 0.
-- Therefore return records where this isn't true to make the test fail.
select
    customer_id
from {{ ref('stg_customers') }}
where customer_id < 0
